<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecepcionReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recepcion_reservas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idReserva');
            $table->string('idRecepcionista',8);
            $table->string('estado',1);

            $table->foreign('idReserva')->references('id')->on('reservas');
            $table->foreign('idRecepcionista')->references('dni')->on('recepcionistas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recepcion_reservas');
    }
}
