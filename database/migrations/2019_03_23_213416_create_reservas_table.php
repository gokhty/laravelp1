<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('idCliente',9);
            $table->string('idAuto',7);
            $table->date('fechaIni');
            $table->date('fechaFin');
            $table->string('estado',1);

             $table->foreign('idCliente')->references('licencia')->on('clientes');
            $table->foreign('idAuto')->references('matricula')->on('automovils');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
