<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App;

class OtroController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function form (Request $request)
    {
    	if($request->isMethod("post"))
    	{
    		 $nom = $request->input("nom");
            $ape = $request->input("ape");


            $fechaIni = date($nom);
            $fechaFin = date($ape);
            //echo count($array1)."<br>";
            //echo $array1."<br>";
            //echo count($notas1);
            //echo count($arreglo)."<br>";
            
            //echo $arreglo."<br>";
           /* if(count($array1)>0){
                $notas = App\Automovil::whereNotIn('matricula',$array1)->get();
            }
            else{
                $notas = App\Automovil::all();
            }*/
            //$notas = App\Reserva::all();
            
    	}
    	else
    	{
    		 $fechaIni = date('2019-03-01');
            $fechaFin = date('2019-03-07');
           
    	}
        $notas1 = App\Reserva::whereBetween('fechaIni',[$fechaIni, $fechaFin])
                                ->orWhereBetween('fechaFin',[$fechaIni, $fechaFin])
                                ->orWhere('fechaIni', '>=', $fechaIni)
                                ->where('fechaFin', '<=', $fechaFin)
                                ->orWhere('fechaIni', '<=', $fechaIni)
                                ->where('fechaFin', '>=', $fechaFin)
                                ->get();     
        if(count($notas1)>0){
                $arreglo = array();
                foreach($notas1 as $indice => $mostrar){
                    array_push($arreglo, $mostrar['idAuto']);
                }
                //$array1= array();
                //array_push($array1, $arreglo);
                $notas = App\Automovil::whereNotIn('matricula',$arreglo)->get();
        }
        else{
            $notas = App\Automovil::all();
        }

       
        

    	//return View("form", ["nom" => $nom, "ape" => $ape, "nac" => $nac, "sexo" => $sexo]);
        return View('form', compact('notas'));
    }
}

https://laraveles.com/foro/viewtopic.php?id=3282