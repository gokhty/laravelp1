<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->isMethod("post"))
        {
             $nom = $request->input("nom");
            $ape = $request->input("ape");


            $fechaIni = date($nom);
            $fechaFin = date($ape);
            
        }
        else
        {
             $fechaIni = date('2019-03-01');
            $fechaFin = date('2019-03-07');
           
        }
        $notas1 = App\Reserva::whereBetween('fechaIni',[$fechaIni, $fechaFin])
                                ->orWhereBetween('fechaFin',[$fechaIni, $fechaFin])
                                ->orWhere('fechaIni', '>=', $fechaIni)
                                ->where('fechaFin', '<=', $fechaFin)
                                ->orWhere('fechaIni', '<=', $fechaIni)
                                ->where('fechaFin', '>=', $fechaFin)
                                ->get();     
        if(count($notas1)>0){
                $arreglo = array();
                foreach($notas1 as $indice => $mostrar){
                    array_push($arreglo, $mostrar['idAuto']);
                }
                //$array1= array();
                //array_push($array1, $arreglo);
                $notas = App\Automovil::whereNotIn('matricula',$arreglo)->get();
        }
        else{
            $notas = App\Automovil::all();
        }

       
        

        //return View("form", ["nom" => $nom, "ape" => $ape, "nac" => $nac, "sexo" => $sexo]);
        return View('home', compact('notas'));
    }
    
}
