1
cd xampp/htdocs
composer create-project laravel/laravel laverelp1 --prefer-dist

2
c:\xampp\apache\conf\extra\httpd-vhosts.conf
<VirtualHost *:80>
  DocumentRoot "C:\xampp\htdocs\laravelp1\public"
  DirectoryIndex index.php
  <Directory "C:\xampp\htdocs\laravelp1">
        AllowOverride All
  </Directory>
  ErrorLog "logs/pruebalaravel-error.log"
  CustomLog "logs/pruebalaravel-access.log" combined
</VirtualHost>

3
htdocs/laravelp1/routes/web.php

4 template
htdocs/laravelp1/resources/views/
crear html css js

5 crear controlador
htdocs/laravelp1/app/Http/Controllers/

6 crear controlador (clase controlador php)
php artisan make:controller CategoriasController
php artisan make:request OtroRequest

7 bd 
(
para versiones antiguas
app/Providers/AppServiceProviders
importar
use Illuminate\Support\Facades\Schema;//para versiones antiguas

metodo boot
Schema::defaultStringLength(191);

 )
migraciones 
las migraciones se crean en esta ruta laravelp1/database/migrations
php artisan migrate

8 crear modelos y migrate a a vez
php artisan make:model Nota -m
